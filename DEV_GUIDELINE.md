## Try and develop on local environment

Before this, you need to install Docker and Docker compose. Please follow links:

https://docs.docker.com/install/

https://docs.docker.com/compose/install/

### Remove previous containers and clean data

`docker-compose down -v`

If you want to remove all images

`docker-compose down -v --rmi all`

### Run containers

All parameters are provided by default. If you want to custom them, please change values in `docker-compose.yml`.

Build containers if code changed

`docker-compose build`

Run containers

`docker-compose up -d`

### Setup WordPress

The first time, you need to setup your WordPress

Go to http://localhost/wordpress and follow the advices.

Notes: the url is different if environment variables: SERVER_NAME and SUB_DIR are setup. It will be http://<SERVER_NAME>/<SUB_DIR> (SUB_DIR must same as WP_PATH).

### Setup MinIO to use S3 plugin

First, to view media on MinIO, edit your /etc/hosts file by appending:

`127.0.0.1  minio`

Second, create the `wordpress` bucket on MinIO

Visit the link http://localhost:9000/minio and login by the credential: `accesskey/secretkey`

Create a bucket with name `wordpress`.

After that, you can upload medias to the bucket of your MinIO S3 server.

## Deploy with an external database and an amazon S3 bucket with IAM role

Create an `.env` file with variables:

* WORDPRESS_DB_HOST= your writer database host
* WORDPRESS_DB_NAME= your wordpress database
* WORDPRESS_DB_USER= your wordpress database user (full privileges access to wordpress database)
* WORDPRESS_DB_PASSWORD= your user password
* REPLICA_DB_HOST= your read repica database host
* REPLICA_DB_USER= your read user
* REPLICA_DB_PASSWORD= your read user password
* S3_PROVIDER=aws
* USE_SERVER_ROLE=true
* S3_BUCKET= your s3 bucket name
* S3_REGION= your region
* BUCKET_URL_FORMAT=cloudfront
* CLOUDFRONT_DOMAIN= your cloudfront 
* S3_OVER_HTTPS=false
* WP_PATH= your wordpress sub path (example: article, your site will be example.com/article)
* SERVER_NAME= your server name, ex: example.com
* SUB_DIR= equals to WP_PATH

If cannot use IAM role, please provide:

* USE_SERVER_ROLE=false
* S3_ACCESS_KEY= your AWS access key id
* S3_SECRET_KEY= your AWS secret key

If cannot use CDN, please set:

* BUCKET_URL_FORMAT=path

Build images

`docker-compose -f docker-compose-deployment.yml build`

Run containers

`docker-compose -f docker-compose-deployment.yml up -d`

Stop and remove containers

`docker-compose -f docker-compose-deployment.yml down`
