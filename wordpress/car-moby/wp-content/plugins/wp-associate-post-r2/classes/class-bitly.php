<?php

namespace WP_Associate_Post_R2;

use WP_Error;

class Bitly {

	const ENDPOINT = 'https://api-ssl.bitly.com/v3/shorten';

	private $access_token    = null;
	private $request_timeout = 10;

	public function __construct( $token ) {
		$this->access_token = $token;
	}

	public function shorten( $long_url ) {
		$params['access_token'] = $this->access_token;
		$params['longUrl']      = $long_url;

		$pairs = array();
		foreach ( $params as $key => $value ) {
			array_push( $pairs, $this->urlencode_RFC3986( $key ) . '=' . $this->urlencode_RFC3986( $value ) );
		}
		$parameter_string = join( '&', $pairs );

		$request_url = self::ENDPOINT . '?' . $parameter_string;

		$response = wp_remote_get(
			$request_url,
			array(
				'timeout'   => $this->request_timeout,
				'sslverify' => false,
			)
		);
		if ( is_wp_error( $response ) ) {
			return $response;
		}
		$json   = wp_remote_retrieve_body( $response );
		$result = json_decode( $json, true );

		if ( '200' != $result['status_code'] ) {
			return new WP_Error( 'api_error', $result['status_code'] . ': ' . $result['status_txt'] );
		}

		return $result['data']['url'];
	}

	// @codingStandardsIgnoreLine
	private function urlencode_RFC3986( $str ) {
		return (string) str_replace( '%7E', '~', rawurlencode( $str ) );
	}

}
