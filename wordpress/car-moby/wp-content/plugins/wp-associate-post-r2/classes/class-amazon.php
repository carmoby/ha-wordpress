<?php

namespace WP_Associate_Post_R2;

use WP_Error;
use DateTime;

class Amazon {

	const ENDPOINT = 'https://webservices.amazon.co.jp/onca/xml';
	const VERSION  = '2013-08-01';

	private $access_key_id     = null;
	private $secret_access_key = null;
	private $associate_tag     = null;
	private $request_timeout   = 10;

	private $search_index = array(
		'Apparel'            => '服＆ファッション小物',
		'Automotive'         => 'カー＆バイク用品',
		'Baby'               => 'ベビー&マタニティ',
		'Beauty'             => 'コスメ',
		'Books'              => '本',
		'Classical'          => 'クラシックミュージック',
		'DVD'                => 'DVD',
		'Electronics'        => '家電・カメラ',
		'ForeignBooks'       => '洋書',
		'Grocery'            => 'お酒・食品・飲料',
		'HealthPersonalCare' => 'ヘルス＆ビューティー',
		'Hobbies'            => 'ホビー',
		'HomeImprovement'    => 'DIY・工具',
		'Industrial'         => '産業・研究開発用品',
		'Jewelry'            => 'ジュエリー',
		'KindleStore'        => 'Kindle',
		'Kitchen'            => 'ホーム＆キッチン',
		'MP3Downloads'       => 'デジタルミュージック',
		'Music'              => 'ミュージック',
		'MusicalInstruments' => '楽器・音響機器',
		'OfficeProducts'     => '文房具・オフィス用品',
		'PCHardware'         => 'パソコン・周辺機器',
		'PetSupplies'        => 'ペット用品',
		'Shoes'              => 'シューズ&バッグ',
		'Software'           => 'PCソフト',
		'SportingGoods'      => 'スポーツ',
		'Toys'               => 'おもちゃ',
		'VideoDownload'      => 'Amazonビデオ',
		'VideoGames'         => 'ゲーム',
		'Watches'            => '腕時計',
	);

	private $product_group_rate = array(
		// ProductGroup                     => Rate(%)
		'Amazon Ereaders'                   => 8,  //Kindleデバイス
		'Amazon SMP'                        => 8,  //FireTV
		'Amazon Tablets'                    => 8,  //Fireデバイス,FireTV
		'Alcoholic Beverage'                => 8,  //お酒
		'Apparel'                           => 8,  //服,ファッション小物
		'Automotive Parts and Accessories'  => 2,  //カー用品,バイク用品
		'Baby Product'                      => 4,  //ベビー・マタニティ用品
		'BAG'                               => 8,  //バッグ
		'Beauty'                            => 5,  //ビューティ
		'BISS Basic'                        => 4,  //産業・研究開発用品
		'Book'                              => 3,  //本(洋書,コミック,雑誌を含む)
		'CE'                                => 2,  //家電＆カメラ アクセサリ
		'Digital Devices 7'                 => 0,  //Dash Button
		'Digital Music Track'               => 8,  //デジタルミュージック
		'Digital Software'                  => 2,  //ゲームダウンロード
		'DVD'                               => 2,  //DVD,Blu-ray
		'eBooks'                            => 8,  //Kindle本(電子書籍)
		'Electronic Gift Card'              => 4,  //ギフト券
		'Gift Card'                         => 4,  //ギフト券
		'Grocery'                           => 8,  //食品&飲料
		'Health and Beauty'                 => 5,  //ヘルス&ビューティ用品
		'Hobby'                             => 3,  //ホビー
		'Home'                              => 3,  //キッチン用品,食器,インテリア,家具,寝具,生活雑貨
		'Home Improvement'                  => 4,  //DIY用品,工具
		'Home Theater'                      => 2,  //テレビ
		'Jewelry'                           => 8,  //ジュエリー
		'Kitchen'                           => 2,  //キッチン家電,生活家電,理美容家電
		'Major Appliances'                  => 2,  //大型家電
		'Mobile Application'                => 8,  //スマホアプリ
		'Movie'                             => 10, //Amazonビデオ
		'Music'                             => 2,  //音楽CD
		'Musical Instruments'               => 2,  //楽器
		'Office Product'                    => 3,  //文房具,オフィス用品
		'Personal Computer'                 => 2,  //PC,プロジェクター
		'Pet Products'                      => 5,  //ペット用品
		'Photography'                       => 2,  //カメラ
		'Shoes'                             => 8,  //シューズ
		'Software'                          => 2,  //PCソフト,PCゲーム
		'Speakers'                          => 2,  //スピーカー
		'Sports'                            => 4,  //スポーツ&アウトドア用品
		'Toy'                               => 3,  //おもちゃ
		'TV Series Episode Video on Demand' => 10, //Amazonビデオ
		'Video Games'                       => 2,  //TVゲーム
		'Watch'                             => 2,  //腕時計
	);

	public function __construct( $access_id, $secret_key, $as_tag ) {
		$this->access_key_id     = $access_id;
		$this->secret_access_key = $secret_key;
		$this->associate_tag     = $as_tag;
	}

	public function get_search_index() {
		return $this->search_index;
	}

	public function item_lookup( $asin ) {
		$params['Operation'] = 'ItemLookup';
		$params['ItemId']    = $asin;
		return $this->send_request( $params );
	}

	public function item_search( $keyword, $page = null, $category = null ) {
		$params['Operation']   = 'ItemSearch';
		$params['ItemPage']    = ! is_null( $page ) ? $page : 1;
		$params['Keywords']    = $keyword;
		$params['SearchIndex'] = ! is_null( $category ) ? $category : 'All';
		return $this->send_request( $params );
	}

	private function send_request( $params ) {
		$params['Service']        = 'AWSECommerceService';
		$params['Version']        = self::VERSION;
		$params['AWSAccessKeyId'] = $this->access_key_id;
		$params['ResponseGroup']  = 'ItemAttributes,Small,Images,OfferSummary';
		$params['Timestamp']      = gmdate( 'Y-m-d\TH:i:s\Z' );
		$params['AssociateTag']   = $this->associate_tag;

		ksort( $params );
		$pairs = array();
		foreach ( $params as $key => $value ) {
			array_push( $pairs, rawurlencode( $key ) . '=' . rawurlencode( $value ) );
		}
		$canonical_query_string = join( '&', $pairs );

		$parsed_url     = parse_url( self::ENDPOINT );
		$string_to_sign = "GET\n{$parsed_url['host']}\n{$parsed_url['path']}\n{$canonical_query_string}";
		$signature      = base64_encode( $this->hmac_sha256( $string_to_sign, $this->secret_access_key ) );

		$request_url = self::ENDPOINT . '?' . $canonical_query_string . '&Signature=' . rawurlencode( $signature );

		$response = wp_remote_get( $request_url, array(
			'httpversion' => '1.1',
			'timeout'     => $this->request_timeout,
			'sslverify'   => false,
		) );
		if ( is_wp_error( $response ) ) {
			return new WP_Error( 'error', $response->get_error_message() );
		}
		if ( wp_remote_retrieve_response_code( $response ) === 503 ) {
			return new WP_Error( 'error_retry' );
		}

		$xml = wp_remote_retrieve_body( $response );

		$xml_object = simplexml_load_string( $xml );
		if ( false === $xml_object ) {
			return new WP_Error( 'error', __( 'Unable to parse XML data.', 'wp-associate-post-r2' ) );
		}

		$xml_namespace = $xml_object->getDocNamespaces();
		$xml_object->registerXPathNamespace( 'x', $xml_namespace[''] );

		if ( $xml_object->xpath( '//x:Error' ) ) {
			$error_code_array    = $xml_object->xpath( '//x:Error[1]/x:Code' );
			$error_code          = (string) $error_code_array[0];
			$error_message_array = $xml_object->xpath( '//x:Error[1]/x:Message' );
			$error_message       = (string) $error_message_array[0];
			switch ( $error_code ) {
				case 'InvalidClientTokenId':
					return new WP_Error( 'error', __( 'Your Access Key ID is not registered for Product Advertising API. Please check your settings.', 'wp-associate-post-r2' ) );
					break;
				case 'SignatureDoesNotMatch':
					return new WP_Error( 'error', __( 'Your Secret Access Key is wrong. Please check your settings.', 'wp-associate-post-r2' ) );
					break;
				case 'AWS.ECommerceService.NoExactMatches':
					return new WP_Error( 'error_zero' );
					break;
			}
			return new WP_Error( 'error', "{$error_message} ({$error_code})" );
		}

		$type     = $params['Operation'];
		$datetime = new DateTime();
		$data     = array();
		foreach ( $xml_object->Items->Item as $item ) {
			$temp                 = array();
			$temp['Service']      = 'amazon';
			$temp['ServiceName']  = 'Amazon';
			$temp['URL']          = (string) $item->DetailPageURL;
			$temp['Date']         = $datetime->format( 'Y-m-d H:i:s' );
			$temp['ID']           = (string) $item->ASIN;
			$temp['Title']        = (string) $item->ItemAttributes->Title;
			$temp['ProductGroup'] = (string) $item->ItemAttributes->ProductGroup;

			if ( $item->OfferSummary->LowestNewPrice && ! in_array( $temp['ProductGroup'], array( 'eBooks', 'Movie' ) ) ) {
				$temp['Price'] = (int) $item->OfferSummary->LowestNewPrice->Amount;
			} else {
				$temp['Price'] = null;
			}

			if ( $item->LargeImage ) {
				$temp['Image'] = (string) $item->LargeImage->URL;
			} elseif ( $item->ImageSets->ImageSet && $item->ImageSets->ImageSet[0]->LargeImage ) {
				$temp['Image'] = (string) $item->ImageSets->ImageSet[0]->LargeImage->URL;
			} else {
				$temp['Image'] = WPAP_PLUGIN_URL . 'images/noimage_500px.png';
			}

			if ( $item->ItemAttributes->EAN ) {
				$temp['ISBNJAN'] = (string) $item->ItemAttributes->EAN;
			}

			if ( $item->ItemAttributes->Author ) {
				$temp['Author'] = $this->to_array( $item->ItemAttributes->Author );
			} elseif ( $item->ItemAttributes->Actor ) {
				$temp['Artist'] = $this->to_array( $item->ItemAttributes->Actor );
			} elseif ( $item->ItemAttributes->Artist ) {
				$temp['Artist'] = $this->to_array( $item->ItemAttributes->Artist );
			}

			if ( in_array( $temp['ProductGroup'], array( 'Book', 'eBooks' ) ) ) {
				if ( $item->ItemAttributes->PublicationDate ) {
					$release_str = (string) $item->ItemAttributes->PublicationDate;
				}
			}
			if ( isset( $release_str ) ) {
				$release_date    = new DateTime( $release_str );
				$temp['Release'] = $release_date->format( 'Y/m/d' );
			}

			if ( 0 === (int) $item->OfferSummary->TotalNew && (int) $item->OfferSummary->TotalUsed > 0 ) {
				$temp['UsedOnly'] = true;
			}

			if ( 'ItemSearch' == $type ) {
				if ( $item->MediumImage ) {
					$temp['Thumbnail'] = (string) $item->MediumImage->URL;
				} elseif ( $item->ImageSets->ImageSet && $item->ImageSets->ImageSet[0]->MediumImage ) {
					$temp['Thumbnail'] = (string) $item->ImageSets->ImageSet[0]->MediumImage->URL;
				} else {
					$temp['Thumbnail'] = WPAP_PLUGIN_URL . 'images/noimage_160px.png';
				}

				if ( array_key_exists( $temp['ProductGroup'], $this->product_group_rate ) ) {
					$temp['Rate'] = $this->product_group_rate[ $temp['ProductGroup'] ];
				} else {
					$temp['Rate'] = 0;
				}
			}
			$data[] = $temp;
		} // End foreach().

		if ( 'ItemLookup' == $type ) {
			$result = $data[0];
		} elseif ( 'ItemSearch' == $type ) {
			$item_total        = (int) $xml_object->Items->TotalResults;
			$page_total_actual = ceil( $item_total / 10 );
			if ( 'All' == $params['SearchIndex'] ) {
				$page_total = ( $page_total_actual < 5 ) ? $page_total_actual : 5;
			} else {
				$page_total = ( $page_total_actual < 10 ) ? $page_total_actual : 10;
			}
			$result = array(
				'item_total' => $item_total,
				'page_now'   => (int) $params['ItemPage'],
				'page_total' => $page_total,
				'data'       => $data,
			);
		}
		return $result;
	}

	private function to_array( $element ) {
		return json_decode( json_encode( $element ), true );
	}

	private function hmac_sha256( $str, $secret_key ) {
		if ( function_exists( 'hash_hmac' ) ) {
			return hash_hmac( 'sha256', $str, $secret_key, true );
		} elseif ( function_exists( 'mhash' ) ) {
			return mhash( MHASH_SHA256, $str, $secret_key );
		}
		return false;
	}

	public static function is_books( $product_group ) {
		$books = array( 'Book', 'DVD', 'Music', 'Software', 'Video Games' );
		if ( in_array( $product_group, $books ) ) {
			return true;
		}
		return false;
	}

}
