<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress');

/** MySQL database username */
define( 'DB_USER', 'wordpress');

/** MySQL database password */
define( 'DB_PASSWORD', 'password');

/** MySQL hostname */
define( 'DB_HOST', 'mysql');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'e5dcd57a48366d305591124fc5bfd2c08661f912');
define( 'SECURE_AUTH_KEY',  'c0488607ef07e77f200b2da1a609a9b1946ea27f');
define( 'LOGGED_IN_KEY',    '813b5a03d15baa3ac61f0363575c2c3b700c0266');
define( 'NONCE_KEY',        'b294467981100102991db204ae3cf5032b78912f');
define( 'AUTH_SALT',        '669121545e9d2dda4b9ae92667b022fe1cf75685');
define( 'SECURE_AUTH_SALT', 'aaff860391551717825bea0984bfd96fa8430871');
define( 'LOGGED_IN_SALT',   'da63c1b25fc21f121ac67e4df9ab3e45fdf67451');
define( 'NONCE_SALT',       'eae1df80fdcc0dbb971c0fbe19cb985f0e431edb');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

// S3 configuration
define( 'AS3CF_SETTINGS', serialize( array(
    'provider' => getenv('S3_PROVIDER'),
    'use-server-roles' => getenv('USE_SERVER_ROLE') == 'true',
    'access-key-id' => getenv('S3_ACCESS_KEY'),
    'secret-access-key' => getenv('S3_SECRET_KEY'),
    'bucket' => getenv('S3_BUCKET'),
    'region' => getenv('S3_REGION'),
    // Automatically copy files to bucket on upload
    'copy-to-s3' => true,
    // Rewrite file URLs to bucket
    'serve-from-s3' => true,
    // Bucket URL format to use ('path', 'cloudfront')
    'domain' => getenv('BUCKET_URL_FORMAT'),
    // Custom domain if 'domain' set to 'cloudfront'
    'cloudfront' => getenv('CLOUDFRONT_DOMAIN'),
    // Serve files over HTTPS
    'force-https' => getenv('S3_OVER_HTTPS') == 'true',
    // Remove the local file version once offloaded to bucket
    'remove-local-file' => true
) ) );

// define Home and Site URL
define( 'WP_HOME', 'http://' . $_SERVER['HTTP_HOST'] . '/' . getenv('WP_PATH') );
define( 'WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/' . getenv('WP_PATH') );


// Force Admin over SSL
// define('FORCE_SSL_ADMIN', true);

// hosted behind a reverse proxy that provides SSL, but is hosted itself without SSL
 if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false) {
     $_SERVER['HTTPS']='on';
 }

// disable install plugin 
define('DISALLOW_FILE_MODS', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

