## Setup AWS Environment

Terms:
* ${AWS_ACCOUNT_ID}: your aws account id
* ${AWS_ACCESS_KEY_ID}: your aws accesskey id
* ${AWS_SECRET_ACCESS_KEY}: your aws secretkey
* ${AWS_REGION}: your aws region
* ${PROJECT_NAME}: your project name, for example: car-moby

### Create Database RDS Aurora

Go to https://console.aws.amazon.com/rds

Create database

* Method: Standard Create
* Engine options
  * Engine type: Amazon Aurora
  * Edition: MySQL compatibility
  * Version: Aurora (MySQL)-5.6.10a
  * Database Location: Regional
* Database features: One writer and multiple readers
* Templates: Production
* Settings
  * DB cluster identifier: Type a name for your DB cluster
  * Credentials Settings
    * Master username: login ID for the master user
    * Master password
    * Confirm password
* DB instance size
  * Memory Optimized classes: db.r5.large (depends on our application data)
  * Availability & durability: Create an Aurora Replica/Reader node in a different AZ
* Connectivity
  * VPC: choose VPC that our apps belong to
* Database authentication: Password authentication

We will have 2 DB instances: writer and read replica.

After creating the database cluster, we will create a database for our WordPress application.

* From an EC2 node in the same VPC as your Aurora DB's VPC, install an MySQL client or run Docker mysql

`docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:5.6`

Access to mysql container

`docker exec -it some-mysql sh`

* Connect to Aurora writer as master user 

`mysql -h <writer-host> -u <master-user> -p`

* Create a database

`create database wordpress;`

* Create users

`create user 'wp-admin'@'%' identified by '<admin-password>';`

`create user 'wp-guest'@'%' identified by '<guest-password>';`

* Grant privileges

`grant all privileges on wordpress.* to 'wp-admin'@'%' identified by 'S9yF7aaVEkDLxxh';`

`grant select on wordpress.* to 'wp-guest'@'%' identified by 'N33pBAZncysvwL6';`

Flush

`flush privileges;`

We will use `wp-admin` to connect to the writer instance, `wp-guest` to connect to the read replica instance.

### Create IAM role for ECS
Go to https://console.aws.amazon.com/iam/home?region=ap-northeast-1#/roles

Create role "ecsInstanceRole": https://docs.aws.amazon.com/batch/latest/userguide/instance_IAM_role.html
Create role "ecsTaskRole": https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_execution_IAM_role.html

### Create S3 bucket
Go to https://s3.console.aws.amazon.com/s3/home?region=ap-northeast-1

create new bucket:
* ${S3_BUCKET} example: nal.car-moby.jp

Setup permission for bucket:

Create a `s3-bucket-policy.json` file from the template `aws/s3-bucket-policy.template` by replacing variables, examples:
* ${AWS_ACCOUNT_ID}: your aws account id
* ${S3_BUCKET}: your aws s3 bucket
* ${EC2_S3_ROLE}: 
    * Create a `s3-policy.json` file from the template `aws/s3-policy.template` by replacing variables
    * then go to IAM > add policy > copy json text to Json editor and save policy.
    * then add policy to role EC2

Click to ${S3_BUCKET} > permissions > Block public access > edit and un-checkbox Block all public access.

Then, Click to Bucket Policy > copy text from s3-bucket-policy.json to editor and save.

Finish: 
* Add s3 policy to ecsInstanceRole. Now ECS EC2 will connect to S3 without access_key and secret_key.
* Add s3 policy to ecsTaskRole. Now ECS docker container will connect to S3 without access_key and secret_key.

### Create ECR repositories

Go to https://console.aws.amazon.com/ecr

Create 2 repositories:
* ${PROJECT_NAME}-wordpress
* ${PROJECT_NAME}-nginx

### Setup a ECS cluster

We will create a ECS cluster with 2 EC2 instances

Go to https://console.aws.amazon.com/ecs

Navigate Clusters > Create New Cluster

Select cluster template: EC2 Linux + Networking

**Configure cluster**

* Cluster name: ${PROJECT_NAME}-cluster
* Instance configuration
  * Provisioning Model: On-Demand Instance
  * EC2 instance type: see https://aws.amazon.com/ec2/pricing/on-demand/
  * Number of instances: 2
  * EC2 Ami Id: Amazon Linux 2 AMI
  * EBS storage (GiB): 22
  * Key pair: choose a key pair if you need to SSH to EC2
* Networking
  * Choose a existed VPC and subnets or Create a new VPC
* Container instance IAM role
  * Container instance IAM role: ecsInstanceRole
* CloudWatch Container Insights: enable

### Setup a Application Load Balancer

Go to https://console.aws.amazon.com/ec2/v2/home?region=ap-northeast-1#SelectCreateELBWizard:

Choose Application Load Balancer and Create with information:

* Name: name of load balancer, ex: ${PROJECT_NAME}-load-balancer
* Scheme: internet-facing
* Ip address type: ipv4
* Listeners
  * HTTP
  * HTTPS
* Availability Zones
  * VPC: choose a existed VPC
  * Availability Zones: Check two subnets from different zones

Next, Configure Security Settings

* Cerfiticate type: Upload a certificate or choose a existed one
* Security policy: leave as default

Next, Configure Security Groups

* Select a existed security group or create one with HTTP, HTTPS allowed.

Next, Configure Routing

* Target group: New target group
* Name: name of target group, ex: ${PROJECT_NAME}-target-group
* Target type: Instance
* Protocol: HTTP
* Port: 80
* Health check
  * Protocol: HTTP
  * Path: /status

Next, Register Targets

* Choose two EC2 instance which belongs created ECS cluster.
* Add to registered on port 80

Next, Review and Create

Config HTTP redirect to HTTPS

* Go to your Load Balancer in EC2 and tab "Listeners"
* Select "View/edit rules" on your HTTP listener
* Delete all rules except for the default one (bottom)
* Edit default rule: choose "Redirect to" as an action, leave everything as default and enter "443" as a port.


### Install AWS CLI

AWS CLI is a commandline tool to interact with AWS enviroment.

To install AWS CLI, please follow the guideline of Amazon:

https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html

https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html

In this guideline, we use version 1 of aws cli.

### Create a task definition

Create a `task-definition.json` file from the template `aws/task-definition.template` by replacing variables, examples:

* AWS_ECR_ACCOUNT_URL: ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com
* WP_PATH: path to your WordPress application, example: your domain is example.com, path is wordpress, so your app should be example.com/wordpress
* ${ECS_TASK_ROLE}: ecsTaskRole
* BUCKET_URL_FORMAT:
    * Use S3 config 'path'
    * Use CDN config 'cloudfront'
* CLOUDFRONT_DOMAIN: if BUCKET_URL_FORMAT config 'cloudfront', add Domain Name or CNAMEs of cloudfront. 

Config log:
Go to https://ap-northeast-1.console.aws.amazon.com/cloudwatch > Log groups > create metric filter. example: nal.car-moby.jp
If you want to cloudwatch log. See 'logConfiguration' in template `aws/task-definition.template`
* ${CLOUD_WATCH_GROUP_LOG}: Your cloudwatch group log. example: nal.car-moby.jp
* ${AWS_REGION}: your aws region

when you config this, cloudwatch will save container log for you.
    

Create the task by aws cli

`aws ecs register-task-definition --cli-input-json file://task-definition.json`

### Create a service

Create a `service-definition.json` file from the template `aws/service-definition.template` by replacing variables.

* ${AWS_TARGET_GROUP_ARN}: your target group ARN, ex: arn:aws:elasticloadbalancing:${AWS_REGION}:${AWS_ACCOUNT_ID}:targetgroup/${TARGET_GROUP_NAME}/${TARGET_GROUP_ID}
* ${ECS_SERVICE_ROLE_ARN}: your ecs role ARN, ex: arn:aws:iam::${AWS_ACCOUNT_ID}:role/aws-service-role/ecs.amazonaws.com/${ECS_SERVICE_ROLE}

You have to enable your target group stickness feature to save web sessions

https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-target-groups.html#sticky-sessions

You need create a ${ECS_SERVICE_ROLE} often named as AWSServiceRoleForECS, please see the guideline 

https://docs.aws.amazon.com/AmazonECS/latest/developerguide/using-service-linked-roles.html

Create the service by aws cli

`aws ecs create-service --cli-input-json file://service-definition.json`

## Setup CircleCI project

### Add Project to CircleCI

Follow this guideline to add a Bitbucket or Github project to CircleCI

https://circleci.com/docs/2.0/project-build/

### Add Environment Variables to Project

Adding following variables to your project on CircleCI

* ${AWS_ACCOUNT_ID}: your aws account id
* ${AWS_ACCESS_KEY_ID}: your aws accesskey id
* ${AWS_SECRET_ACCESS_KEY}: your aws secretkey
* ${AWS_REGION}: your aws region
* ${AWS_ECR_ACCOUNT_URL}: your aws ecs url
* ${ECR_WORDPRESS_REPO}: your wordpress image repository, ex: ${PROJECT_NAME}-wordpress
* ${ECR_NGINX_REPO}: your nginx image repository, ex: ${PROJECT_NAME}-nginx 
* ${ECS_CLUSTER}: your ecs cluster to deploy the project, ex: ${PROJECT_NAME}-cluster
* ${ECS_SERVICE}: your ecs service, ex: ${PROJECT_NAME}-service
* ${ROLE_NAME}: Your role for aws STS

In the Menu, enter the section WORKFLOWS, we will see the list of workflows, each flow contains three jobs:

* assume_role: Get AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY from AWS STS and set to env
* build-and-push-wordpress: build and push wordpress image to Amazon ECR repository.
* build-and-push-nginx: build and push nginx image to Amazon ECR repository.
* aws-ecs/deploy-service-update: create a new task revision with new images and update service's task. ECS cluster will rolling update the service that makes the service is always available.

### Rollback to an old version

If new version has a problem, we can rollback to an old version by re-run `aws-ecs/deploy-service-update` job of the old workflow. In order to to that, choose an old workflow, view detail, choose job `aws-ecs/deploy-service-update`, view detail and you triggers `Rerun job with SSH` from the job detail url.


## Setup ElastiCache

### Step 1: Set up ElastiCache for Memcached instance

First, you need to launch a [Memcached instance](https://docs.aws.amazon.com/AmazonElastiCache/latest/mem-ug/GettingStarted.CreateCluster.html).

Memcached settings
* Cluster engine: Memcached
* Name: Name of memcached server
* Node type: Memory save cache. You can see [price](https://docs.aws.amazon.com/AmazonElastiCache/latest/mem-ug/nodes-select-size.html#CacheNodes.SelectSize)
* Number of nodes: partitions of your data

Advanced Memcached settings
* Subnet group: Create new
* VPC: choose same VPC of ECS
* Subnet: choose same Subnet of ECS
* Security groups: choose same Security groups of EC2

then, click create. After a few minutes the cluster status will become 'Available'. Make a note of the Configuration Endpoint.

### Step 2: Set up WordPress Instance Access to the Memcached Instance

In Security groups of WordPress EC2, Add Custom TCP Rule with: 
* Port 11211
* Source: Security groups of ALB

### Step 3: Connect to WordPress and Install the Memcached Client

We setting config in Dockerfile. Run docker-compose build and docker-compose up.

Or You can config manual follow :
* Copy amazon-elasticache-cluster-client.so to /usr/local/lib/php/extensions/amazon-elasticache-cluster-client.so

  `docker cp ./wordpress/extensions/amazon-elasticache-cluster-client.so wordpress:/usr/local/lib/php/extensions/`
  
* Create file docker-php-ext-memcached.ini with content: extension=/usr/local/lib/php/extensions/amazon-elasticache-cluster-client.so
    
    after copy to /usr/local/etc/php/conf.d/
    
  `docker cp docker-php-ext-memcached.ini wordpress:/usr/local/etc/php/conf.d/`
  
### Step 4: Enable Caching
We add W3 Total Cache plugin to WordPress. You can use it to setting cache.
We suggest some config you should enable because it will improve your site’s performance.

In W3 Total Cache’s General Settings:  
* Enable Page Cache, Object Cache, Browser Cache
* Cache method: chose memcached

#### Config Page cache

Under General 
* Cache front page: Checked
* Cache feeds: Checked
* Cache SSL: you can enable this box even if you’re not currently using SSL.
* Don’t cache pages for logged in users: Checked

Under Cache Preload
* Automatically prime the page cache: Checked
* Update interval: 900 seconds
* Pages per interval: 10
* Sitemap URL: The actual link to your sitemap if you have one. Typically “yourdomain.com/sitemap.xml” for most SEO plugins.
* Preload the post cache upon publish events: Checked

Under Advanced
* Memcached hostname:port / IP:port: Paste to EndPoint of elasticache you create at Step 1.


#### Config Object cache
Under Advanced
* Memcached hostname:port / IP:port: Paste to EndPoint of elasticache you create at Step 1.
* Default lifetime of cache objects: 180 seconds
* Garbage collection interval: 3600 seconds

#### Config Browser cache
Under General
* Set Last-Modified header: Checked
* Set expires header: Checked
* Set cache control header: Checked
* Set entity tag (eTag): Checked
* Set W3 Total Cache header: Checked
* Enable HTTP (gzip) compression: Checked
