**WordPress with Nginx in a containerized approach**

A WordPress application contains:

* Nginx container: as a Web server, serve static files and routing.
* PHP-FPM container: as PHP Fast CGI, run php files.
* Database: MySQL based (MySQL, MariaDB on local environment or Amazon Aurora on production environment).
* File storage: S3 compliant (MinIO on local environment or Amazon S3 on production environment).

The local develop environment will use docker-compose to run containers. See the guideline in DEV_GUIDELINE.md.

The production enviroment will use Amazon ECS (Elastic Container Service) to run containers. See the guideline in **docs** folder.
